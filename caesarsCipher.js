function rot13(str) { // LBH QVQ VG!
  var sid = '';
  for(var i =0;i<str.length;i++){
      // for char only and the diffrence should be greater than 65;
      if(str.charCodeAt(i)>=65){
            if(str.charCodeAt(i)-13>=65)
                 sid+=String.fromCharCode(str.charCodeAt(i)-13);
            else
                 sid+=String.fromCharCode(str.charCodeAt(i)+13);        
      }
       else
          // non char symbols only   
          sid+=str[i]; 
  }
  return sid;
}

// Change the inputs below to test
rot13("SERR PBQR PNZC");
