function checkCashRegister(price, cash, cid) {
  var change = Math.abs(cash-price);
  var curr = [0.01,0.05,0.10,0.25,1,5,10,20,100],amount=[];
  var temp,final,stat ={
    status:'',
    change:[] 
  };
  curr.push(change);
  curr.sort( (a,b) => a-b);
  //get pos of change due
  let changePos = curr.indexOf(change);
  // remove the change due
  curr.splice(changePos,1);
  //store the value till change due pos
  for(var i =0 ; i<changePos;i++)
    amount.push(cid[i][1]);
  //calculate the final sum untill change due pos and compare it..
  final = amount.reduce( (a,b) => a+b );
  //for status open
  if(final > change){
      stat.status = "OPEN";
      for(var i = changePos-1;i>=0;i--){
          // console.log(amount[i]);
          if(cid[i][1]!==0){
            temp = curr[i];
            while(temp<amount[i] &&temp <change)
                temp+=curr[i];
            if(temp > change)
                temp-=curr[i];
            change = parseFloat((change-temp).toFixed(2));
            temp = parseFloat((temp).toFixed(2));
            console.log(temp);
            if(temp!==0)
               stat.change.push([cid[i][0],temp]);
          }
      }
  }
  //status closed
  else if(final===change){
      stat.status = "CLOSED";
      stat.change = [...cid]
  }
  // status insufficient
  else{
      stat.status = "INSUFFICIENT_FUNDS";
      stat.change = [];
  }
  console.log(stat);
  return stat;
}
// Example cash-in-drawer array:
// [["PENNY", 1.01],
// ["NICKEL", 2.05],
// ["DIME", 3.1],
// ["QUARTER", 4.25],
// ["ONE", 90],
// ["FIVE", 55],
// ["TEN", 20],
// ["TWENTY", 60],
// ["ONE HUNDRED", 100]]

checkCashRegister(3.26, 100, [["PENNY", 1.01], ["NICKEL", 2.05], ["DIME", 3.1], ["QUARTER", 4.25], ["ONE", 90], ["FIVE", 55], ["TEN", 20], ["TWENTY", 60], ["ONE HUNDRED", 100]]);